package lab3;

public class DriverLicence {
	
	private String driverName;
	private String driverID;
	private String expirationDate;
	private String bloodType;
	private int penalty = 0;
	private boolean status = true;
	private int limit = 100;
	
	public DriverLicence(String driverName, String driverID, String expirationDate, String bloodType) {
		
		this.driverName = driverName;
		this.driverID = driverID;
		this.expirationDate = expirationDate;
		this.bloodType = bloodType;
		this.penalty = penalty;
		this.status = status;
		this.limit = limit;
		
		
		
		
		
		
	}
	public void increasePenalty(int penalty) {
		this.penalty = this.penalty + penalty;
		if (this.penalty >= limit) {
			this.status = false;
		}
		if (this.penalty > limit) {
			this.penalty = limit;
		}
		System.out.println("Penalty = " + this.penalty);
		System.out.println("Status = " + this.status);
	}
	public void decreasePenalty(int penalty) {
		this.penalty = this.penalty - penalty;
		if (this.penalty < 20) {
			this.status = true;
		}
		if (this.penalty < 0 ) {
			this.penalty = 0;
		}
		System.out.println("Penalty = " + this.penalty);
		System.out.println("Status = " + this.status);
	}
	public String getDriverName() {
		return driverName;
	}
	public String getDriverID() {
		return driverID;
	}
	public String getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}
	public String getBloodType() {
		return bloodType;
	}
	public int getPenalty() {
		return penalty;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	
	public String toString() {
		return "DriverLicence \n[DriverName=" + driverName + ", DriverID=" + driverID + ", ExpirationDate="
				+ expirationDate + ", BloodType=" + bloodType + ", Penalty=" + penalty + ", Status=" + status
				+ "]";
	}
	public void report() {
		System.out.println(toString());
	}
	

}
